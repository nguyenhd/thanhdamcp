<?php

namespace app\controllers;

use Yii;

use yii\web\Session;
use yii\helpers\ArrayHelper;
use yii\db\Query;
use yii\db\QueryBuilder;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\SignupForm;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\NguoiDung;

class SiteController extends Controller
{
    public $layout = 'master';
    
    /**
     * @inheritdoc
     */
    //public function behaviors()
//    {
//        return [
//            'access' => [
//                'class' => AccessControl::className(),
//                'only' => ['logout', 'signup'],
//                'rules' => [
//                    [
//                        'actions' => ['signup'],
//                        'allow' => true,
//                        'roles' => ['?'],
//                    ],
//                    [
//                        'actions' => ['logout'],
//                        'allow' => true,
//                        'roles' => ['@'],
//                    ],
//                ],
//            ],
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'logout' => ['post'],
//                ],
//            ],
//        ];
//    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionLogout()
    {
        $this->layout = 'main';
        $request = Yii::$app->request;  
        $model = new NguoiDung();
        unset(Yii::$app->session['user']);
        return $this->redirect(['site/logan', 'user'=>$model]); 
    }
     
    public function actionRegister(){
        $this->layout = 'main';
        $request = Yii::$app->request;  
        $model = new NguoiDung();
        if(Yii::$app->session['user'])
            return $this->redirect(['site/index']);
        if($request->isGet)
            return $this->render('register', ['user'=>$model]);
        else{
            $data = $request->post('NguoiDung'); 
            if(NguoiDung::findOne(['ten_dang_nhap'=>$data['ten_dang_nhap']])){
//                Yii::$app->getSession()->setFlash('notification', 'Tên người dùng này đã tồn tại. Vui lòng tạo tên người dùng mới.');   
                Yii::$app->getSession()->setFlash('notification', [
                    'error' => 'Tên người dùng này đã tồn tại. Vui lòng tạo tên người dùng khác!',
                    'ten_dang_nhap' => $data['ten_dang_nhap'],
                    'email'=>$data['email'],
                ]);   
                return $this->redirect(['site/register']); 
            }
            if(NguoiDung::findOne(['email'=>$data['email']])){
//                Yii::$app->getSession()->setFlash('notification', 'Tên người dùng này đã tồn tại. Vui lòng tạo tên người dùng mới.');   
                Yii::$app->getSession()->setFlash('notification', [
                    'error' => 'Email này đã được sử dụng!',
                    'ten_dang_nhap' => $data['ten_dang_nhap'],
                    'email'=>$data['email'],
                ]);   
                return $this->redirect(['site/register']); 
            }
            $max = NguoiDung::find()->max('id_nguoi_dung');
            $max = (int)$max;
            $model->id_nguoi_dung = $max+1;
            $model->ten_dang_nhap = $data['ten_dang_nhap'];
            $model->email = $data['email'];
            $hash = Yii::$app->getSecurity()->generatePasswordHash($data['mat_khau']);
            $model->mat_khau = $hash; 
            $model->quyen = 'true';
            $model->trang_thai='true';   
            var_dump($model)   ;
            if($model->save()){
                return $this->render('index');
            }else{
                die('Save did not success');
            }         
         }
    }
    public function actionLogan()
    {
        $this->layout = 'main';
        $request = Yii::$app->request;  
        $model = new NguoiDung();
        if(Yii::$app->session['user'])
            return $this->redirect(['site/index']);
        if($request->isGet)
            return $this->render('logan', ['user'=>$model]);
        else{
            $data = $request->post('NguoiDung'); 
            $user = NguoiDung::find()->where(['ten_dang_nhap'=>$data['ten_dang_nhap']])->one(); 
            if(($user!=null)) {
                if (Yii::$app->getSecurity()->validatePassword($data['mat_khau'], $user->mat_khau)){
                    Yii::$app->session['user'] = $user;
                    return $this->redirect(['site/index']);
                }   else{
                    Yii::$app->getSession()->setFlash('notification', 'Sai mật khẩu! Vui lòng kiểm tra lại'); 
                }
            }else  
                Yii::$app->getSession()->setFlash('notification', 'Sai tên đăng nhập! Vui lòng kiểm tra lại');
        }
        return $this->redirect(['site/logan']);
    }


}
