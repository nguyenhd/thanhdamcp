<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Logan';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Vui lòng điền đầy đủ thông tin để đăng nhập</p>

    <div class="site-contact">
    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id'=>'contact-form']);?>
            <?= $form->field($user, 'ten_dang_nhap')->label('Tên đăng nhập') ?> 
            <?= $form->field($user, 'mat_khau')->passwordInput(['value'=>'12345678'])->label('Mật khẩu') ?>
            <div class="form-group">
                <span style="color: red"><?= Yii::$app->getSession()->getFlash('notification'); ?></span>
            </div>
            <div class="form-group">
                <?=Html::submitButton('Submit', ['class'=>'btn btn-success', 'name'=>'contact-button'])?>
            </div>
            <?php ActiveForm::end()?>
        </div>
    </div>
</div>
</div>
