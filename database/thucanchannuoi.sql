-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 19, 2016 at 06:47 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `thucanchannuoi`
--

-- --------------------------------------------------------

--
-- Table structure for table `chitiethoadon`
--

CREATE TABLE `chitiethoadon` (
  `id_LSGD` bigint(20) NOT NULL,
  `id_hang` int(10) NOT NULL,
  `so_luong` int(11) DEFAULT NULL,
  `gia_ban` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `chitietphieunhap`
--

CREATE TABLE `chitietphieunhap` (
  `id_phieu_nhap` bigint(20) NOT NULL,
  `id_hang` int(10) NOT NULL,
  `gia_nhap` int(10) DEFAULT NULL,
  `so_luong` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `hang`
--

CREATE TABLE `hang` (
  `id_hang` int(10) NOT NULL,
  `ten_hang` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_trong_luong` int(10) DEFAULT NULL,
  `don_vi_tinh` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `so_luong_ton` int(10) DEFAULT NULL,
  `gia_niem_yet` bigint(10) DEFAULT NULL,
  `gia_vay_no` bigint(10) DEFAULT NULL,
  `gia_chan_nuoi` bigint(10) DEFAULT NULL,
  `chiet_khau` int(11) DEFAULT NULL,
  `mo_ta` text COLLATE utf8_unicode_ci,
  `trang_thai` binary(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `hoadon`
--

CREATE TABLE `hoadon` (
  `id_LSGD` bigint(20) NOT NULL,
  `tong_tien` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chiet_khau` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `khachhang`
--

CREATE TABLE `khachhang` (
  `id_khach_hang` int(10) NOT NULL,
  `ten_khach_hang` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `so_dien_thoai` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dia_chi` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `loai_khach_hang` binary(2) DEFAULT NULL,
  `so_du_no` bigint(20) DEFAULT NULL,
  `trang_thai` binary(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `lichsugiaodich`
--

CREATE TABLE `lichsugiaodich` (
  `id_LSGD` bigint(20) NOT NULL,
  `ngay_lap` date DEFAULT NULL,
  `id_khach_hang` int(10) DEFAULT NULL,
  `id_nguoi_dung` int(10) DEFAULT NULL,
  `kieu_giao_dich` binary(2) DEFAULT NULL,
  `so_tien_tra` bigint(15) DEFAULT NULL,
  `trang_thai` bigint(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `loaihang`
--

CREATE TABLE `loaihang` (
  `id_loai_hang` int(10) NOT NULL,
  `ten_loai_hang` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `trang_thai` binary(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `nguoidung`
--

CREATE TABLE `nguoidung` (
  `id_nguoi_dung` int(10) NOT NULL,
  `ten_dang_nhap` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mat_khau` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quyen` binary(2) DEFAULT NULL,
  `trang_thai` binary(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `phieunhap`
--

CREATE TABLE `phieunhap` (
  `id_phieu_nhap` bigint(20) NOT NULL,
  `ngay_lap` date DEFAULT NULL,
  `tong_tien` bigint(15) DEFAULT NULL,
  `id_nguoi_dung` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `trang_thai` binary(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `trongluong`
--

CREATE TABLE `trongluong` (
  `id_trong_luong` int(10) NOT NULL,
  `trong_luong` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cuoc_van_chuyen` bigint(50) DEFAULT NULL,
  `trang_thai` binary(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chitiethoadon`
--
ALTER TABLE `chitiethoadon`
  ADD PRIMARY KEY (`id_LSGD`,`id_hang`);

--
-- Indexes for table `chitietphieunhap`
--
ALTER TABLE `chitietphieunhap`
  ADD PRIMARY KEY (`id_hang`,`id_phieu_nhap`);

--
-- Indexes for table `hang`
--
ALTER TABLE `hang`
  ADD PRIMARY KEY (`id_hang`);

--
-- Indexes for table `hoadon`
--
ALTER TABLE `hoadon`
  ADD PRIMARY KEY (`id_LSGD`);

--
-- Indexes for table `khachhang`
--
ALTER TABLE `khachhang`
  ADD PRIMARY KEY (`id_khach_hang`);

--
-- Indexes for table `lichsugiaodich`
--
ALTER TABLE `lichsugiaodich`
  ADD PRIMARY KEY (`id_LSGD`);

--
-- Indexes for table `loaihang`
--
ALTER TABLE `loaihang`
  ADD PRIMARY KEY (`id_loai_hang`);

--
-- Indexes for table `nguoidung`
--
ALTER TABLE `nguoidung`
  ADD PRIMARY KEY (`id_nguoi_dung`);

--
-- Indexes for table `phieunhap`
--
ALTER TABLE `phieunhap`
  ADD PRIMARY KEY (`id_phieu_nhap`);

--
-- Indexes for table `trongluong`
--
ALTER TABLE `trongluong`
  ADD PRIMARY KEY (`id_trong_luong`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
