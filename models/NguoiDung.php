<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "nguoidung".
 *
 * @property integer $id_nguoi_dung
 * @property string $ten_dang_nhap
 * @property string $mat_khau
 * @property string $email
 * @property string $quyen
 * @property string $trang_thai
 * @property string $authKey
 */
class NguoiDung extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nguoidung';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_nguoi_dung', 'ten_dang_nhap', 'email', 'mat_khau'], 'required', 'message'=>'Không được để trống trường này!'],
            [['id_nguoi_dung'], 'integer'],
            [['email'], 'email', 'message'=>'Email không đúng định dạng!'],
            [['ten_dang_nhap', 'email'], 'string', 'max' => 50],
            [['mat_khau'], 'string', 'max' => 70],
            [['quyen', 'trang_thai'], 'string', 'max' => 6],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_nguoi_dung' => 'Id Nguoi Dung',
            'ten_dang_nhap' => 'Ten Dang Nhap',
            'mat_khau' => 'Mat Khau',
            'email' => 'Email',
            'quyen' => 'Quyen',
            'trang_thai' => 'Trang Thai',
        ];
    }
}
