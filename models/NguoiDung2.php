<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\helpers\Security;
use yii\web\IdentityInterface;


/**
 * This is the model class for table "nguoi_dung".
 *
 * @property integer $id_nguoi_dung
 * @property string $ten_nguoi_dung
 * @property integer $mat_khau
 * @property integer $trang_thai
 * @property string $authKey
 */
class NguoiDung extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nguoi_dung';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_nguoi_dung', 'ten_nguoi_dung', 'mat_khau', 'authKey'], 'required'],
            [['id_nguoi_dung', 'trang_thai'], 'integer'],
            [['ten_nguoi_dung', 'authKey', 'mat_khau'], 'string', 'max' => 70],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_nguoi_dung' => 'Id Nguoi Dung',
            'ten_nguoi_dung' => 'Ten Nguoi Dung',
            'mat_khau' => 'Mat Khau',
            'trang_thai' => 'Trang Thai',
            'authKey' => 'Auth Key',        
        ];
    }
    /**
     * Finds an identity by the given ID.
     * @param string|integer $id the ID to be looked for
     * @return IdentityInterface the identity object that matches the given ID.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentity($id){
        return self::findOne($id);
    }
//    public static function findIdentity($id){
//       return self::findOne(['ten_nguoi_dung'=>$username]); 
//    }







  //  public function getAuthKey()
//    {
//        throw new NotSupportedException();//You should not implement this method if you don't have authKey column in your database
//    }
//     
//    public function validateAuthKey($authKey)
//    {
//       throw new NotSupportedException();//You should not implement this method if you don't have authKey column in your database
//    }






    /**
     * Finds an identity by the given token.
     * @param mixed $token the token to be looked for
     * @param mixed $type the type of the token. The value of this parameter depends on the implementation.
     * For example, [[\yii\filters\auth\HttpBearerAuth]] will set this parameter to be `yii\filters\auth\HttpBearerAuth`.
     * @return IdentityInterface the identity object that matches the given token.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentityByAccessToken($token, $type = null){
        throw new \yii\base\NotSupportedException();    
    }

    /**
     * Returns an ID that can uniquely identify a user identity.
     * @return string|integer an ID that uniquely identifies a user identity.
     */
    public function getId(){
        return $this->mat_khau;
    }

    /**
     * Returns a key that can be used to check the validity of a given identity ID.
     *
     * The key should be unique for each individual user, and should be persistent
     * so that it can be used to check the validity of the user identity.
     *
     * The space of such keys should be big enough to defeat potential identity attacks.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @return string a key that is used to check the validity of a given identity ID.
     * @see validateAuthKey()
     */
    public function getAuthKey(){
        return $this->authKey;
    }

    /**
     * Validates the given auth key.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @param string $authKey the given auth key
     * @return boolean whether the given auth key is valid.
     * @see getAuthKey()
     */
    public function validateAuthKey($authKey){
        return $this->authKey === $authKey;
    }
    public static function findByUsername($username){
        return self::findOne(['ten_nguoi_dung'=>$username]);
    }
    public function validatePassword($password)
    {
        return $this->mat_khau === $password;
    }
    
    /**moi add them**
    public static function findIdentityByAccessToken($token, $type = null)
    {
          return static::findOne(['access_token' => $token]);
    }
    public static function findByPasswordResetToken($token)
    {
        $expire = \Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        if ($timestamp + $expire < time()) {
            // token expired
            return null;
        }
 
        return static::findOne([
            'password_reset_token' => $token
        ]);
    }
    /**moi add them**/
    public function setPassword($password)
    {
//        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
        $this->mat_khau = Yii::$app->security->generatePasswordHash($password);
    }
    public function generateAuthKey()
    {
        $this->authKey = Yii::$app->security->generateRandomString();
    }
}
